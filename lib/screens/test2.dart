import 'dart:io';

import 'package:flutter/material.dart';

import 'package:webview_cookie_manager/webview_cookie_manager.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:login_signup_ui_starter/storage.dart';

void main() {
  runApp(HomeScreen());
}

class HomeScreen extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<HomeScreen> {
  myFunction() async {
    var token = await SecureStorage.readData('token');
    // print(token);
    return token;
  }

  final cookieManager = WebviewCookieManager();

  final String _url = 'https://app.seller.mn';
  final String cookieValue = 'some-cookie-value';
  final String domain = 'https://app.seller.mn';
  final String cookieName = 'some_cookie_name';
  var sessionCookie = WebViewCookie(
    name: 'my_session_cookie',
    value: 'cookie_value',
    domain: 'https://app.seller.mn',
  );

  @override
  void initState() {
    super.initState();
    cookieManager.clearCookies();
    myFunction();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
          actions: [
            IconButton(
              icon: Icon(Icons.ac_unit),
              onPressed: () async {
                // TEST CODE
                await cookieManager.getCookies(null);
              },
            )
          ],
        ),
        body: WebView(
          initialUrl: _url,
          javascriptMode: JavascriptMode.unrestricted,
          initialCookies: [sessionCookie],
          onProgress: (controller) async {
            await cookieManager.setCookies([
              Cookie('token', await myFunction())
                ..domain = domain
                ..expires = DateTime.now().add(Duration(days: 10))
                ..httpOnly = true
            ]);
          },
          onPageStarted: (controller) async {
            await cookieManager.setCookies([
              Cookie('token', await myFunction())
                ..domain = domain
                ..expires = DateTime.now().add(Duration(days: 10))
                ..httpOnly = false
            ]);
          },
          onWebViewCreated: (controller) async {
            await cookieManager.setCookies([
              Cookie('token', await myFunction())
                ..domain = domain
                ..expires = DateTime.now().add(Duration(days: 10))
                ..httpOnly = true
            ]);
          },
          onPageFinished: (controler) async {
            final gotCookies = await cookieManager.getCookies(_url);
            print(gotCookies);
            for (var item in gotCookies) {
              print(item);
            }
          },
        ),
      ),
    );
  }
}
