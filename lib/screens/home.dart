import 'dart:io';

import 'package:flutter/material.dart';

import 'package:webview_cookie_manager/webview_cookie_manager.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:login_signup_ui_starter/storage.dart';

void main() {
  runApp(HomeScreen());
}

class HomeScreen extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<HomeScreen> {
  myFunction() async {
    var token = await SecureStorage.readData('token');
    // print(token);
    return token;
  }

  final cookieManager = WebviewCookieManager();

  final String _url = 'https://app.seller.mn/#';
  final String domain = 'https://app.seller.mn/#';

  @override
  void initState() {
    super.initState();
    cookieManager.clearCookies();
    myFunction();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        child: WebView(
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (controller) async {
            await cookieManager.setCookies([
              Cookie('token', await myFunction())
                ..domain = domain
                ..expires = DateTime.now().add(Duration(days: 10))
                ..httpOnly = true
            ]);
            controller.loadUrl(domain + "?token=" + await myFunction());
          },
          onPageFinished: (controler) async {
            final gotCookies = await cookieManager.getCookies(_url);
            for (var item in gotCookies) {
              print(item);
            }
          },
        ),
      ),
    );
  }
}
