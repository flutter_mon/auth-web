// import 'package:login_signup_ui_starter/screens/auth/auth_screen.dart';
// import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:login_signup_ui_starter/screens/login.dart';
import 'package:login_signup_ui_starter/screens/register.dart';
import 'package:login_signup_ui_starter/screens/resetpass.dart';
import 'package:login_signup_ui_starter/screens/home.dart';

void main() {
  runApp(
    GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: 'login',
      title: 'OMS',
      routes: {
        'login': (context) => MyLogin(),
        'register': (context) => myRegister(),
        'forgot': (context) => resetPassword(),
        'home': (context) => HomeScreen(),
      },
    ),
  );
}
// main() async {
//   runApp(GetMaterialApp(
//     debugShowCheckedModeBanner: false,
//     home: AuthScreen(),
//   ));
// }
