class ApiEndPoints {
  static final String baseUrl = 'https://api.seller.mn/api/v1';
  static _AuthEndPoints authEndpoints = _AuthEndPoints();
}

class _AuthEndPoints {
  final String registerEmail = '/auth/';
  final String loginEmail = '/auth/login';
}
